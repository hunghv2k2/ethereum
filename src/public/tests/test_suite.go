package tests

import (
	"context"
	"gitlab.com/backend/ethereum/public/bootstrap"
	"gitlab.com/golibs-starter/golib"
	"gitlab.com/golibs-starter/golib-data/testutil"
	"gitlab.com/golibs-starter/golib-migrate"
	"gitlab.com/golibs-starter/golib-test"
	"go.uber.org/fx"
)

func init() {
	err := fx.New(
		bootstrap.All(),
		golibdataTestUtil.EnableDatabaseTestUtil(),
		golibmigrate.MigrationOpt(),
		golib.ProvidePropsOption(golib.WithActiveProfiles([]string{"testing"})),
		golib.ProvidePropsOption(golib.WithPaths([]string{"../config/"})),
		golibtest.EnableWebTestUtil(),
	).Start(context.Background())
	if err != nil {
		panic(err)
	}
}
