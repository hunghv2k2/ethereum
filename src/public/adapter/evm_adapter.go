package adapter

import (
	"context"
	"errors"
	"fmt"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/ethclient"
	"gitlab.com/backend/ethereum/public/properties"
	"math"
	"math/big"
)

type EvmAdapter struct {
	ethClient        *ethclient.Client
	submitProperties *properties.SubmitProperties
}

func (e *EvmAdapter) Transfer(recipient string) error {
	account := common.HexToAddress(recipient)
	balance, err := e.ethClient.BalanceAt(context.Background(), account, nil)
	if err != nil {
		return errors.New("error when get balance")
	}

	if balance.Cmp(big.NewInt(0)) == -1 {
		return errors.New("not enough balance")
	}

	fbalance := new(big.Float)
	fbalance.SetString(balance.String())
	ethValue := new(big.Float).Quo(fbalance, big.NewFloat(math.Pow10(18)))
	fmt.Println(ethValue)

	pendingBalance, err := e.ethClient.PendingBalanceAt(context.Background(), account)
	if err != nil {
		return err
	}
	fmt.Println(pendingBalance)

	return nil
}
