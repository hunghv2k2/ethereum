package adapter

type BlockchainPort interface {
	Transfer(string) error
}
