package bootstrap

import (
	"github.com/go-playground/validator/v10"
	"gitlab.com/backend/ethereum/public/client"
	"gitlab.com/backend/ethereum/public/controllers"
	"gitlab.com/backend/ethereum/public/properties"
	"gitlab.com/backend/ethereum/public/routers"
	"gitlab.com/golibs-starter/golib"
	"gitlab.com/golibs-starter/golib-gin"
	"go.uber.org/fx"
)

// All register all constructors for fx container
func All() fx.Option {
	return fx.Options(
		golib.AppOpt(),
		golib.PropertiesOpt(),
		golib.LoggingOpt(),
		golib.EventOpt(),
		golib.BuildInfoOpt(Version, CommitHash, BuildTime),
		golib.ActuatorEndpointOpt(),

		golib.HttpClientOpt(),

		//provide
		fx.Provide(client.NewEthClient),
		fx.Provide(controllers.NewTransactionController),
		fx.Provide(validator.New),

		golib.ProvideProps(properties.NewSwaggerProperties),
		golib.ProvideProps(properties.NewSubmitProperties),

		golibgin.GinHttpServerOpt(),

		fx.Invoke(routers.RegisterHandlers),
		fx.Invoke(routers.RegisterGinRouters),
		//fx.Provide(gin.New),

		// Graceful shutdown.
		// OnStop hooks will run in reverse order.
		// golib.OnStopEventOpt() MUST be first
		golib.OnStopEventOpt(),
		golibgin.OnStopHttpServerOpt(),
	)
}
