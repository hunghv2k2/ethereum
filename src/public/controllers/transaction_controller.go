package controllers

import (
	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator/v10"
	"gitlab.com/backend/ethereum/public/adapter"
	"gitlab.com/backend/ethereum/public/request"
	"gitlab.com/golibs-starter/golib/exception"
	"gitlab.com/golibs-starter/golib/log"
	"gitlab.com/golibs-starter/golib/web/response"
	"net/http"
)

type TransactionController struct {
	blockchainPort adapter.BlockchainPort
	validator      *validator.Validate
}

func NewTransactionController(
	blockchainPort adapter.BlockchainPort, validator *validator.Validate,
) *TransactionController {
	return &TransactionController{
		blockchainPort: blockchainPort,
		validator:      validator,
	}
}

func (c *TransactionController) Transfer(ctx *gin.Context) {
	var req request.TransferRequest
	if err := ctx.Bind(&req); err != nil {
		log.Error(ctx, "Cannot bind request, err [%s]", err)
		response.WriteError(ctx.Writer, exception.BadRequest)
		return
	}

	if err := c.validator.Struct(&req); err != nil {
		log.Error(ctx, "Bad request, err ", err)
		response.WriteError(ctx.Writer, exception.BadRequest)
		return
	}

	if err := c.blockchainPort.Transfer(req.ToAddress); err != nil {
		log.Error(ctx, "Transfer failed, err [%v]", err)
		response.WriteError(ctx.Writer, err)
		return
	}

	ctx.Status(http.StatusAccepted)
}
