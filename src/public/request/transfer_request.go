package request

type TransferRequest struct {
	ToAddress string `json:"to_address" binding:"eth_addr"`
}
