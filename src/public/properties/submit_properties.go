package properties

import (
	"gitlab.com/golibs-starter/golib/config"
)

type SubmitProperties struct {
	URL     string
	ChainID int64
}

func NewSubmitProperties(loader config.Loader) (*SubmitProperties, error) {
	props := SubmitProperties{}
	err := loader.Bind(&props)
	return &props, err
}

func (s *SubmitProperties) Prefix() string {
	return "app.chain"
}
