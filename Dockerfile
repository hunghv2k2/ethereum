FROM golang:1.17-alpine AS builder
ARG GITLAB_USER
ARG GITLAB_ACCESS_TOKEN
ARG MODULE
ARG BUILD_VERSION
ARG BUILD_COMMIT_HASH
ARG BUILD_TIME
ARG BS_PKG=gitlab.com/backend/ethereum/$MODULE/bootstrap
ENV GOPRIVATE=gitlab.com
RUN apk add --no-cache git
RUN git config --global url."https://$GITLAB_USER:$GITLAB_ACCESS_TOKEN@gitlab.com/".insteadOf "https://gitlab.com/"
COPY ./src /go/src
WORKDIR /go/src/$MODULE
RUN env GOOS=linux GOARCH=amd64 CGO_ENABLED=0 go build -o ./application \
	-ldflags="-X '$BS_PKG.Version=$BUILD_VERSION' -X '$BS_PKG.CommitHash=$BUILD_COMMIT_HASH' -X '$BS_PKG.BuildTime=$BUILD_TIME'"

FROM alpine:3.15
ARG MODULE
COPY --from=builder /go/src/$MODULE/config /app/config
COPY --from=builder /go/src/$MODULE/application /app
EXPOSE 8080
WORKDIR /app
ENTRYPOINT ["./application"]